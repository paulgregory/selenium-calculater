﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class CalculatorProgram
    {
        public int Add(int p1, int p2)
        {
             return p1+p2;
        }

        public int Multiply(int p1, int p2)
        {
            return p1 * p2;
        }
    }
}
