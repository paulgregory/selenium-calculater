﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;
using Calculator;

namespace DemoCalculator
{
    [Binding]
    public class CalculatorSteps
    {
        private int _result;
        [When(@"I add (.*) and (.*)")]
        public void WhenIAddAnd(int p0, int p1)
        {
            _result = new CalculatorProgram().Add(p0, p1);
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(int p0)
        {
            Assert.AreEqual(p0, _result);
        }
    }
}
